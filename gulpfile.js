/**
 * Created by jefflu on 1/27/16.
 */
//dependencies
var gulp  = require('gulp');
var util  = require('gulp-util');
var watch = require('gulp-watch');
var nodemon = require('gulp-nodemon');
var gls = require('gulp-live-server');
var del = require('del');
var ts = require('gulp-typescript');
var gtsc = require('gulp-tsc');
var typescript = require('typescript');
var sourcemaps = require('gulp-sourcemaps');
var SystemBuilder = require('systemjs-builder');
var runSequence = require('run-sequence');
var connect = require('gulp-connect');
var open = require('gulp-open');
var express = require('gulp-express');
var gutil = require('gulp-util');
var shell = require('gulp-shell');
var path = require('path');
var karma = require('karma');
var karmaParseConfig = require('karma/lib/config').parseConfig;
var tsconfig = require('./tsconfig.json');
// Only compile changed files
tsconfig.compilerOptions.noExternalResolve = true;
tsconfig.compilerOptions.declaration = true;
tsconfig.compilerOptions.isolatedModules = true;

var systemConfig = require('./config.js');
var karmaConfig = require('./karma.config.js');

var srcBase = 'docroot/sites/all/modules';
var wuBase = srcBase+'/wu';
var wuSrc = 'src/app',
  wuBuild = wuBase+'/build',
  wuMap = wuBase+'/maps',
  publicRoot = 'docroot/public',
  publicDocRoot = 'docroot/public/docroot';

var tasks = {
  'default': 'default',
  cleanAll : 'clean-all',
  typeScript: 'typescript-compile',
  startWebServer: 'ttart-web-server',
  liveServer: 'live-server',
  openBrowser: 'open-browser',
  testBrowser: 'test-browser',
  unitTest: 'unit-test',
  watch: 'watch',
  karmaWatch: 'karma-watch',
  watcherRebuild: 'watcher-rebuild',
  browserSync: 'browser-sync',
  tsc: 'tsc',
  copy: 'copy',
  tidyUp: 'tidy-up',
  karmaRun: 'karma-run'
};

var serverOptions = {
  root: 'src',
  port: 8008,
  livereload: true,
};

function runKarma(configFilePath, options, cb) {

  configFilePath = path.resolve(configFilePath);

  var Server = karma.Server;
  var log=gutil.log, colors=gutil.colors;
  var config = karmaParseConfig(configFilePath, {});

  Object.keys(options).forEach(function(key) {
    config[key] = options[key];
  });

  new Server(config, function(exitCode) {
    log('Karma has exited with ' + colors.red(exitCode));
    cb();
    process.exit(exitCode);
  }).start();
}

/** single run */
gulp.task('test', function(cb) {
  runKarma('karma.config.js', {
    autoWatch: false,
    singleRun: true
  }, cb);
});

/** continuous ... using karma to watch (feel free to circumvent that;) */
gulp.task('test-dev', function(cb) {
  //runKarma('karma.config.js', {
  //  autoWatch: true,
  //  singleRun: false
  //}, cb);
  runSequence(
    tasks.typeScript,
    tasks.tidyUp,
    tasks.karmaWatch,
    function() {
      runKarma('karma.config.js', {
        autoWatch: true,
        singleRun: false
      }, cb);
    }
  );
});

// compiles *.ts files by tsconfig.json file and creates sourcemap filse
gulp.task(tasks.typeScript, function () {
  var tsProject = ts.createProject(tsconfig);
  console.log('compile ts files.', tsconfig);
  return gulp.src([
    'typings/**/*.d.ts',
    'src/**/*.ts'
  ])
    .pipe(sourcemaps.init())
    .pipe(ts(tsProject))
    .pipe(sourcemaps.write('./', { includeContent: false, sourceRoot: '/'+wuSrc }))
    .pipe(gulp.dest('./dist/src'));
});

gulp.task(tasks.tidyUp, function() {
  runSequence(
    tasks.copy,
    tasks.cleanAll
  );
});

// Copy compiled files from dist to src
gulp.task(tasks.copy, function () {
  return gulp.src(['./dist/src/**/**.js', './dist/src/**/**.map'], { base: "./dist" })
    .pipe(gulp.dest('./'));
});

//  clean all generated/compiled files
gulp.task(tasks.cleanAll, function () {
  return del(['./dist']);
});

gulp.task(tasks.liveServer, function() {
  //1. serve with default settings
  var server = gls.static('/', 8888);
  server.start();

  //use gulp.watch to trigger server actions(notify, start or stop)
  gulp.watch(['src/**/*.css', 'src/**/*.html', 'src/**/*.js'], function (file) {
    server.notify.apply(server, [file]);
  });
  gulp.watch(['src/**/*.ts'], function(file) {
    console.log('File changes detected.');
    gulp.task(tasks.typeScript);
  });
});

// starts web server
gulp.task(tasks.startWebServer, function () {
  //connect.server(serverOptions);
  return express.run([
    'server.js'
  ]);
});

gulp.task(tasks.openBrowser, function () {
  gulp.src('index.html')
    .pipe(open('./', { url: 'http://localhost:' + serverOptions.port }));
});
gulp.task(tasks.testBrowser, function () {
  gulp.src('')
    .pipe(open({ uri: 'http://localhost:8888/src/unit-tests.html' }));
});

gulp.task(tasks.unitTest, function() {
  runSequence(
    tasks.typeScript,
    tasks.liveServer,
    tasks.testBrowser
  );
});

gulp.task(tasks.tsc, function() {
  console.log('directory:', __dirname, process.cwd());
  tsconfig.emitError= false;
  gulp.src(['src/**/*.ts'])
  .pipe(gtsc(tsconfig))
  .pipe(gulp.dest('./'));
});

gulp.task(tasks.watcherRebuild, function(cb) {
  runSequence(
    tasks.typeScript,
    tasks.tidyUp
  );
  cb();
});

gulp.task(tasks.karmaRun, function() {
  karma.runner.run(karmaConfig);
});

//live reload server
gulp.task('build', ['compile:app']);

gulp.task('server', ['build', 'default'], function() {
  nodemon({
    watch: [
      'src'
    ],
    ext: 'js ts json html'
  })
    .on('restart', function () {
      gulp.run(['build']);
    });
});

// watchers
gulp.task(tasks.watch, function () {
  gulp.watch([wuSrc+'/**/**.ts', wuSrc+'/**/**.html', wuSrc+'/**/**.css'], [tasks.watcherRebuild, reload]);
});
gulp.task(tasks.karmaWatch, function () {
  gulp.watch([wuSrc+'/**/*.ts', wuSrc+'/**/*.html', wuSrc+'/**/*.css'], [tasks.watcherRebuild]);
});

//default task
gulp.task('default', function() {
  gulp.watch(['src/**/*.ts'], ['compile:app']);
  gulp.watch(['src/**/.js', 'src/**/*.html'], ['copy:src']);
});
