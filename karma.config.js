/**
 * Created by jefflu on 2/3/16.
 */
module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['systemjs', 'jspm', 'jasmine'],
    plugins:[
      "karma-jspm",
      'karma-jasmine',
      'karma-chrome-launcher',
      "karma-systemjs"
    ],
    systemjs: {
      configFile: "config.js",
      //serveFiles: [
      //  'src/**/*'
      //],
      includeFiles: [
        'jspm_packages/npm/reflect-metadata@0.1.3/Reflect.js'
      ],
      config: {
        paths: {
          'angular2': 'jspm_packages/angular2@2.0.0-beta.2',
          'systemjs': 'jspm_packages/system.js',
          'system-polyfills': 'jspm_packages/system-polyfills.js',
          'typescript': 'node_modules/typescript/lib/typescript.js',
          'es6-module-loader': 'node_modules/es6-module-loader/dist/es6-module-loader.js'
        },
        packages: {
          "src": {
            //"defaultExtension": false
          }
        },
        testFileSuffix: '.spec.js'
      }
    },
    files: [
      'src/**/**.js'
    ],
    exclude: [
      'src/**/**.ts'
    ],
    jspm: {
      config: './config.js',
      packages: 'jspm_packages/',
      loadFiles: ['src/**/**.spec.js'],
      serveFiles: ['src/**/**.js', 'src/**/**.map', 'src/**/**.ts']
    }   ,
    proxies : { // avoid Karma's ./base virtual directory
      '/src/': '/base/src'
      //'/test/': '/base/test/',
      //'/jspm_packages/': '/base/jspm_packages/'
    },
    preprocessors: {

    },
    reporters: ['progress'],
    port: 9876,
    // enable / disable colors in the output (reporters and logs)
    colors: true,
    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_ERROR,
    singleRun: false,
    browsers: ['Chrome']
  })
}