/**
 * Created by jefflu on 1/18/16.
 */
import {describe, expect, it, xit, inject, beforeEachProviders} from 'angular2/testing';

describe('1st tests', () => {
  it('true is true', () => expect(true).toEqual(true));

  it('null is not the same thing as undefined',
    () => expect(null).not.toEqual(undefined)
  );
});