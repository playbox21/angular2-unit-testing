/**
 * Created by jefflu on 12/13/15.
 */

import {Component} from 'angular2/core';
import {HTTP_PROVIDERS} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/fromArray';
import 'rxjs/add/operator/concatMap';

import { _settings } from '../../settings';
import {WuService} from '../../services/wuService';
import {WuFavoriteBar} from './wu-favorite-bar';

var cityMap = new WeakMap<WuFavorites, Array<any>>();

@Component({
  selector: 'wu-favorites',
  providers: [WuService, HTTP_PROVIDERS],
  templateUrl: _settings.srcPath + '/components/wu-favorites/wu-favorites.html',
  directives: [WuFavoriteBar]
})
export class WuFavorites {
  favorites: Array<any[]>;
  wuService: any;

  constructor(wuService: WuService) {
    let obj = [],
      self = this;
    this.wuService = wuService;
    cityMap.set(this, this.init());
    this.getPopCities()
      .subscribe(function(res) {
        obj.push(res);
      }, function(err) {
        console.log(err);
      }, function() {
        let units = wuService.getCookie('Units') === 'metric' ? ' °C ' : ' °F ';
        obj.map(res => {
          cityMap.get(self).some(city => {
            let location = res.response.location,
              obs = res['current_observation'];
            if(city.name === location.city) {
              city['condition'] = obs.temperature + units + obs.condition;
              return true;
            }
          });
        });
        self.favorites = cityMap.get(self);
        return obj
      });

  }

  private init(): Array<any> {

    // need to get cities from somewhere, is it from cookies?
    return [
      {
        name: 'New York',
        niceName: 'New York, NY',
        url: '/weather-forecast/zmw:10001.5.99999',
        zmw: 'zmw:10001.5.99999',
        condition: '84.4 °F Clear',
        template: 'lic.html',
        ttl: '.json?ttl=300'
      },
      {
        name: 'London',
        niceName: 'London, UK',
        url: '/weather-forecast/zmw:00000.1.03772',
        zmw: 'zmw:00000.1.03772',
        condition: '66 °F Light Rain',
        template: 'lic.html',
        ttl: '.json?ttl=300'
      },
      {
        name: 'Chicago',
        niceName: 'Chicago, IL',
        url: '/weather-forecast/zmw:60290.1.99999',
        zmw: 'zmw:60290.1.99999',
        condition: '83.9 °F Partly Cloudy',
        template: 'lic.html',
        ttl: '.json?ttl=300'
      },
      {
        name: 'Boston',
        niceName: 'Boston, MA',
        url: '/weather-forecast/zmw:02101.1.99999',
        zmw: 'zmw:02101.1.99999',
        condition: '72.9 °F Overcast',
        template: 'lic.html',
        ttl: '.json?ttl=300'
      },
      {
        name: 'Houston',
        niceName: 'Houston, TX',
        url: '/weather-forecast/zmw:77001.1.99999',
        zmw: 'zmw:77001.1.99999',
        condition: '95.4 °F Partly Cloudy',
        template: 'lic.html',
        ttl: '.json?ttl=300'
      },
      {
        name: 'San Francisco',
        niceName: 'San Francisco, CA',
        url: '/weather-forecast/zmw:94101.1.99999',
        zmw: 'zmw:94101.1.99999',
        condition: '71.9 °F Partly Cloudy',
        template: 'lic.html',
        ttl: '.json?ttl=300'
      }
    ];
  }

  getPopCities() {
    let wuService = this.wuService;
    let cookies = wuService.getUserCookies();
    let urls = [], obj = [];

    cityMap.get(this).map((city) => {
      urls.push(wuService.tokenReplace(wuService.endPoints.popCity + city.zmw + city.ttl, {units: cookies['Units'], lang: cookies['LANGAS']}));
    });
    return Observable.fromArray(urls)
      .concatMap(url => {
        return wuService.get(url)
    })
    .map(response => response.json())
  }

}

