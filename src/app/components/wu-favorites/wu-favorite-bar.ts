/**
 * Created by jefflu on 12/15/15.
 */
import {Component, Input, Output, EventEmitter} from 'angular2/core';
import { _settings } from '../../settings';

@Component ({
  selector: 'wuFavoriteBar',
  templateUrl: _settings.srcPath + '/components/wu-favorites/lic.html',
  providers: []
  // outputs: ['selectOrder']
})
export class WuFavoriteBar {
  @Input('favorites')
    favorites: any;
  @Output('selectCity')
    selectCity = new EventEmitter();
  template: String;
  onSelect(city: any){
    this.selectCity.next(city);
  }

  constructor() {
    this.template = _settings.srcPath + '/components/wu-favorites/lic.html';
  }
}
