/**
 * Created by jefflu on 1/26/16.
 */
import {InitCapsPipe} from '../init-caps-pipe';

describe('InitCapsPipe', () => {
  let pipe: InitCapsPipe;

  beforeEach(() => {
    pipe = new InitCapsPipe();
  });

  it('transform "cat" to "Cat', () => {
    expect(pipe.transform('cat')).toEqual('Cat');
  });

  it('transfrom "cat woman" to "Cat Woman"', () => {
    expect(pipe.transform('cat woman')).toEqual('Cat Woman');
  });

  it('Cat Woman is still Cat Woman', () => {
    expect(pipe.transform('Cat Woman')).toEqual('Cat Woman');
  });
});
