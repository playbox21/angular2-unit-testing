/**
 * Created by jefflu on 2/7/16.
 */
import {describe, expect, it, xit, inject, beforeEachProviders} from 'angular2/testing';
import {Http, Response} from 'angular2/http';
import {WuService} from '../wuService';

let wuService: WuService;

beforeEach(() => {
  wuService = new WuService(Http);
});
describe('WuService', () => {
  //beforeEachProviders(() => [
  //  Http,
  //  WuService
  //]);
  //it('should get blog endpoint', inject([WuService], (wuService) => {
  //  let blogEndPoint = wuService.get(wuService.endPoints.blogFeed);
  //  expect(blogEndPoint).toBe('http://api-ak.wunderground.com/api/d8585d80376a429e/blogs/v:2.0/q/zmw:94108.1.99999.json');
  //}));
  it('should get blog endpoint', function() {
    let blogEndPoint = wuService.get(wuService.endPoints.blogFeed);
    expect(blogEndPoint).toBe('http://api-ak.wunderground.com/api/d8585d80376a429e/blogs/v:2.0/q/zmw:94108.1.99999.json');
  });
});
