import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';

var httpMap = new WeakMap<WuService, Http>();
@Injectable()
export class WuService
{
  endPoints: any;
  region: any;
  mainApp: any;

  constructor(http:Http){
    httpMap.set(this, http);
    this.endPoints = {
      activeStormFeed: 'http://api-ak.wunderground.com/api/8ab9284dd387405c/currenthurricane/view.json?clickmaps=1',
      blogFeed: 'http://api-ak.wunderground.com/api/d8585d80376a429e/blogs/v:2.0/q/zmw:94108.1.99999.json',
      popCity: 'http://api-ak.wunderground.com/api/eb7a37c339cfd624/conditions/alerts/v:2.0/lang:${lang}/units:${units}/q/'
    }
    this.region = {
      global: 'global',
      northatlantic: 'atlantic',
      eastpacific: 'eastern-pacific',
      westpacific: 'wetern-pacific',
      indianocean: 'indian-ocean',
      atlantic: 'northatlantic',
      'eastern-pacific': 'eastpacific',
      'wetern-pacific': 'westpacific',
      'indian-ocean': 'indianocean'
    }
  }

  setApp(app) {
    this.mainApp = app;
  }

  getApp() {
    return this.mainApp;
  }

  tokenReplace(template, obj) {
    return template.replace(/\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g, function (match, key) {
      return obj[key];
    });
  }

  get(url: String) {
    return httpMap.get(this).get(url);
  }

  getBlog() {
    return httpMap.get(this).get(this.endPoints.blogFeed);
  }

  getUserCookies() {
    let type = {
      'Units': 'english',
      'LANGAS': 'EN'
    };
    let cookies = [];
    for(let i in type) {
      var cookie = this.getCookie(i);
      cookies[i] = cookie ? cookie : type[i];
    }
    return cookies;
  }

  getCookie(cname: String) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i=0; i<ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return null;
  }

  getURLParameter(name, search) {
    var match = new RegExp('[?&]' + name + '=([^&]*)').exec(search ? search : window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  }

  getCamelCase(name) {
    return name.toLowerCase().replace(/-(.)/g, function(match, group1) {
      return group1.toUpperCase();
    });
  }

}
