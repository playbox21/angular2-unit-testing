/**
 * Created by jefflu on 1/18/16.
 */
import {Hero} from '../hero';
describe('Hero', () => {
  it('has name given in the constructor', function() {
    let hero = new Hero(1, 'Super Cat');
    expect(hero.name).toEqual('Super Cat');
  });

  it('has the id given in the contructor', function() {
    let hero = new Hero(1, 'Super Cat');
    expect(hero.id).toEqual(1);
  });
});