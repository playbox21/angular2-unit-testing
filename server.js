/**
 * Created by jefflu on 12/30/15.
 */
var express = require('express');
var bodyParser = require('body-parser');
var _ = require('underscore');
var curlReq = require('curlrequest');
var cheerio = require("cheerio");
var deferred = require('deferred');
var http = require('http'),
  fs = require('fs');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('src'));
app.use(require('connect-livereload')());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(function(req, res, next) {
  var originalUrl = req.originalUrl;
  if(originalUrl.match(/\/wu\/build\//) || originalUrl.match(/\/api\//)) {
    return next();
  }
  var url = req.protocol + '://' + req.get('host') + req.originalUrl;
  console.log(url);
  curlRequest(req.originalUrl).then(function(content) {
    //console.log('content: ', content);
    res.send(content);
  });

});

app.get('/', function (request, response) {
  response.sendfile('docroot/public/index.html');
});

app.get('/api/sample', function(request, response){
  response.send({name:"Ravi"});
});

app.get('/api/achievements/:type', function(request, response) {
  var achievements=[
    {
      title:'Received Microsoft MVP Award',
      type:'major'
    },
    {
      title:'Approved as SitePoint author',
      type:'major'
    },
    {
      title:'Approved as DotnetCurry author',
      type:'major'
    },
    {
      title:'Mention on ASP.NET',
      type:'medium'
    },
    {
      title:'First article published on SitePoint',
      type:'minor'
    },
    {
      title:'Got a side project',
      type:'minor'
    },
    {
      title:'Boss patted me for my work',
      type:'minor'
    }
  ];

  response.send(_.filter(achievements, function(a){
    return a.type === request.params.type;
  }));
});

//app.get('/ng2', function(req, res) {
//  var url = req.protocol + '://' + req.get('host') + req.originalUrl;
//  console.log(url);
//  curlRequest(req.originalUrl).then(function(content) {
//    console.log('content: ', content);
//    res.send(content);
//  });
//});

app.listen(8008, function () {
  console.log('Express server started!!!');
});

function curlRequest(path) {
  var df = deferred();
  var url = 'http://wu.dev'+path;
  //var url = 'http://playbox21.vagrantshare.com'+path;
  console.log('Drupal: ', url);
  curlReq.request({url: url}, function(err, data) {
    if (!err) {
      var $ = cheerio.load(data);
      var meta = $('meta');
      var title = $('title').toString();
      var wrapper = $('div.content-wrap');
      meta = meta ? meta.toString() : '';
      title = title ? title.toString() : '';
      wrapper = wrapper ? wrapper.toString() : '';
      if(wrapper) {
        //console.log(title, meta, wrapper);
        getApp(path).then(function(html) {
          if(html) {
            var output = tokenReplace(html, {app: path, title: title, meta: meta});
            console.log(output);
            df.resolve(output);
          } else {
            console.log('failed');
            df.resolve();
          }
        });
      } else {
        df.resolve();
      }
    }
  });
  return df.promise;
}

function getApp(path) {
  var df = deferred();
  fs.readFile('./docroot/public/bootstrap.html', function(err, html) {
    if(!err) {
      //console.log('read file:', html.toString());
      df.resolve(html.toString());
    } else {
      df.resolve();
    }
  });
  return df.promise;
}

function tokenReplace(template, obj) {
  return template.replace(/\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g, function (match, key) {
    return obj[key];
  });
}
