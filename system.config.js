/**
 * Created by jefflu on 1/27/16.
 */
System.config({
  baseURL: "/",
  defaultJSExtensions: true,
  transpiler: "typescript",
  packages: {
    "src": {
      "defaultExtension": "js"
    }
  },
  map: {
    'typescript': 'node_modules/typescript/lib/typescript.js',
    'angular2': 'node_modules/angular2',
    'angular2-universal-preview': 'node_modules/angular2-universal-preview/dist/client/index',
    'rxjs': 'node_modules/rxjs'
  }
});
